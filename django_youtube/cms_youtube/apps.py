import urllib.request
from django.apps import AppConfig
from xml.sax import ContentHandler, parse

videos = {}


class Handler(ContentHandler):

    def __init__(self):
        super().__init__()
        self.inEntry = False
        self.inContent = False
        self.Content = ""
        self.Title = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif name == 'title' and self.inEntry:
            self.inContent = True
        elif name == 'link' and self.inEntry:
            self.Content += attrs.getValue('href')

    def characters(self, content):
        if self.inEntry and self.inContent:
            self.Title += content

    def endElement(self, name):
        global videos
        if name == 'entry':
            videos[self.Title] = self.Content
            self.inEntry = False
            self.Title = ""
            self.Content = ""
        elif name == 'title':
            self.inContent = False


class CmsYoutubeConfig(AppConfig, Handler):
    name = "cms_youtube"

    def ready(self):
        url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg"
        xmlStream = urllib.request.urlopen(url)
        parse(xmlStream, Handler())
        return videos
