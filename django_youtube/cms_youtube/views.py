from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from django.views.decorators.csrf import csrf_exempt
from . import apps

# Create your views here.

selected_videos = {}


def movecontent(origin, destiny, title, url):
    try:
        del (origin[title])
        destiny[title] = url
    except KeyError:
        print('Key ' + title + 'does not exist')


@csrf_exempt
def index(request):
    if request.method == "POST":
        title = request.POST['title']
        url = request.POST['url']
        print(title)
        if request.POST['selection'] == 'yes':
            movecontent(apps.videos, selected_videos, title, url)
        elif request.POST['selection'] == 'no':
            movecontent(selected_videos, apps.videos, title, url)
    return HttpResponse(render(request, 'cms_youtube/page.html',
                               {'selected_videos': selected_videos, 'not_selected_videos': apps.videos}))


def not_found(request, string):
    return HttpResponse(render(request, 'cms_youtube/not_found.html'))
